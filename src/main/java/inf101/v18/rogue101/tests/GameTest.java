package inf101.v18.rogue101.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import inf101.v18.grid.GridDirection;
import inf101.v18.grid.ILocation;
import inf101.v18.rogue101.examples.Rabbit;
import inf101.v18.rogue101.game.Game;
import inf101.v18.rogue101.game.IllegalMoveException;
import inf101.v18.rogue101.map.IMapView;
import inf101.v18.rogue101.objects.IItem;
import inf101.v18.rogue101.objects.IPlayer;
import inf101.v18.rogue101.solution.Player;
import javafx.scene.input.KeyCode;

class GameTest {
	public static String TEST_MAP = "4 3\n" //
			+ "####\n" //
			+ "#@R#\n" //
			+ "####\n" //
	;

	@Test
	void testAttack() {
		// new game with our test map
		Game game = new Game(TEST_MAP);
		// pick (3,2) as the "current" position; this is where the player is on the
		// test map, so it'll set up the player and return it
		IMapView map = game.getMap();
		Rabbit rabbit = (Rabbit) map.getAll(map.getLocation(2, 1)).get(0);
		IPlayer player = (IPlayer) game.setCurrent(1, 1);

		assertNotNull(rabbit);
		assertNotNull(player);

		try {
			player.keyPressed(game, KeyCode.RIGHT);
			String lastMessage = game.getLastMessage();
			assertTrue(lastMessage.contains("attack") || lastMessage.contains("hits"), "moving right should result in attack");
		} catch (IllegalMoveException e) {
			fail("Move right should throw illegalmoveexception");
		}
		// Rabbit rabbit = new Rabbit();
		// IPlayer player = new Player();
		// map.add(map.getLocation(2,1), rabbit);
		// map.add(map.getLocation(1,1), player);
	}
}
