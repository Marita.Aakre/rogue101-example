package inf101.v18.rogue101.tests;

import static org.junit.Assert.*;

import org.junit.jupiter.api.Test;

import inf101.v18.grid.ILocation;
import inf101.v18.rogue101.examples.Carrot;
import inf101.v18.rogue101.examples.Rabbit;
import inf101.v18.rogue101.map.GameMap;
import inf101.v18.rogue101.objects.IItem;

class GameMapTest {

	@Test
	void testAddActuallyAdds() {
		GameMap gameMap = new GameMap(20, 20);
		ILocation location = gameMap.getLocation(10, 10);
		Rabbit rabbit = new Rabbit();
		gameMap.add(location, rabbit);
		assertTrue(gameMap.getAll(location).contains(rabbit));
	}

	@Test
	void testAddAddsOne() {
		GameMap gameMap = new GameMap(20, 20);
		ILocation location = gameMap.getLocation(10, 10);
		addAddsOneProperty(gameMap, location, new Rabbit());
	}
	@Test
	void testAddAddsOne1() {
		GameMap gameMap = new GameMap(20, 20);
		ILocation location = gameMap.getLocation(10, 10);
		gameMap.add(location, new Carrot()/* noe som er større enn rabbit */);
		addAddsOneProperty(gameMap, location, new Rabbit());
	}
	@Test
	void testAddAddsOne2() {
		GameMap gameMap = new GameMap(20, 20);
		ILocation location = gameMap.getLocation(10, 10);
		gameMap.add(location, new Carrot());
		addAddsOneProperty(gameMap, location, new Rabbit());
	}

	void addAddsOneProperty(GameMap map, ILocation loc, IItem item) {
		int size = map.getAll(loc).size();
		map.add(loc, item);
		assertEquals(size+1, map.getAll(loc).size());
	}
	@Test
	void testSortedAdd() {
		GameMap gameMap = new GameMap(20, 20);
		ILocation location = gameMap.getLocation(10, 10);
		// TODO:
		fail("Not yet implemented");
	}

}
