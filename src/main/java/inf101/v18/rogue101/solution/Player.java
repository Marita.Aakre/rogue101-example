package inf101.v18.rogue101.solution;

import java.util.List;

import inf101.v18.gfx.gfxmode.ITurtle;
import inf101.v18.gfx.gfxmode.Point;
import inf101.v18.grid.GridDirection;
import inf101.v18.grid.ILocation;
import inf101.v18.rogue101.effects.Rainbow;
import inf101.v18.rogue101.game.IGame;
import inf101.v18.rogue101.objects.IItem;
import inf101.v18.rogue101.objects.IPlayer;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.util.Duration;

public class Player implements IPlayer {
	private int hp = getMaxHealth();
	private Timeline tl;
	private Rainbow rainbow;
	private GridDirection facing = GridDirection.EAST;

	private void drop(IGame game) {
		// TODO Auto-generated method stub

	}

	@Override
	public int getAttack() {
		return 10;
	}

	@Override
	public int getCurrentHealth() {
		return hp;
	}

	@Override
	public int getDamage() {
		return 10;
	}

	@Override
	public int getDefence() {
		return 10;
	}

	@Override
	public int getMaxHealth() {
		return 10;
	}

	@Override
	public String getName() {
		return "nerdy game player";
	}

	@Override
	public int getSize() {
		return 10;
	}

	@Override
	public String getSymbol() {
		return "\u001b[31m" + "@" + "\u001b[0m";
	}

	@Override
	public int handleDamage(IGame game, IItem source, int amount) {
		hp -= amount;
		game.displayMessage("Aaaargh!!! Got hit by " + source.getName());
		return amount;
	}

	@Override
	public void keyPressed(IGame game, KeyCode key) {
		if (key == KeyCode.LEFT) {
			tryToMove(game, GridDirection.WEST);
		} else if (key == KeyCode.RIGHT) {
			tryToMove(game, GridDirection.EAST);
		} else if (key == KeyCode.UP) {
			tryToMove(game, GridDirection.NORTH);
		} else if (key == KeyCode.DOWN) {
			tryToMove(game, GridDirection.SOUTH);
		} else if (key == KeyCode.P) {
			pickUp(game);
		} else if (key == KeyCode.D) {
			drop(game);
		}

		game.formatStatus("HP: %d", hp);
	}

	private void pickUp(IGame game) {
		// TODO Auto-generated method stub

	}

	private void tryToMove(IGame game, GridDirection dir) {
		facing = dir;
		if (game.canGo(dir)) {
			if (rainbow != null)
				rainbow.play(
						new Point((0.5+ game.getLocation().getX()) * 16.0, (0.5 + game.getLocation().getY()) * 32.0),
						facing.getDegrees());

			game.move(dir);
			String items = "";
			for (IItem item : game.getLocalItems()) {
				if (!items.equals(""))
					items += ", ";
				items += item.getArticle() + " " + item.getName();
			}
			if (items.equals(""))
				game.displayMessage("There is nothing here");
			else
				game.displayMessage("You see: " + items);
		} else if(game.getLocation().canGo(dir)) { 
			ILocation otherLoc = game.getLocation(dir);
			List<IItem> allThingsAtOtherLoc = game.getMap().getAll(otherLoc);
			if(!allThingsAtOtherLoc.isEmpty()) {
//				game.attack(dir, allThingsAtOtherLoc.get(0));
			} else {
				throw new IllegalStateException("this shouldn't happen!");
			}
		}
		else {
			game.displayMessage("ouch!");
		}
	}

	@Override
	public boolean draw(ITurtle painter, double w, double h) {
		if (rainbow == null)
			rainbow = new Rainbow(painter, 300, 50, 50);
		// rainbow.draw(painter, 1.0);
		return false;
	}
}
