package inf101.v18.gfx.anim;

public interface IAnimJob {
	boolean isRunning();
	boolean isPaused();
	void pause();
	void stop();
	void unpause();
	boolean isStopped();
	long getLastStepTime();
	long getRunningTime();
}
