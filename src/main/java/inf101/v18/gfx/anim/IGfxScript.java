package inf101.v18.gfx.anim;

import inf101.v18.gfx.IPaintLayer;

public interface IGfxScript<T extends IPaintLayer> extends IAnimScript {
	IAnimScript play(T gfx);

	T getGfx();
	void setGfx(T gfx);
	IGfxScript<T> chain(IGfxScript<T> next);

}
