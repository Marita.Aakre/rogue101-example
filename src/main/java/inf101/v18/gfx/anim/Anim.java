package inf101.v18.gfx.anim;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;

import inf101.v18.gfx.IPaintLayer;
import inf101.v18.gfx.gfxmode.ITurtle;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.Transition;
import javafx.event.ActionEvent;
import javafx.util.Duration;

/**
 * Utility class for managing animations.
 * <p>
 * This is a <em>singleton</em> class; there will only be one instance of it.
 * You can't call the constructor; instead, you should call
 * {@link #getInstance()} to obtain an Anim object.
 * <p>
 * Use {@link #play(Predicate, Object)} to have a piece of code called at
 * regular intervals (typically every 1/10, 1/30 or 1/60 second). This is used
 * for animations or other jobs that will run for an indefinite time period. The
 * argument will tell you how many milliseconds has passed since last time, and
 * you can stop further processing by returning <code>false</code>.
 * 
 * @author anya
 *
 */
public class Anim {
	protected static Anim instance = null;
	protected Map<Object, AnimJob> jobs = new IdentityHashMap<>();
	protected final Timeline timeline;
	/**
	 * Number of steps taken.
	 */
	protected long frameCount = 0;
	/**
	 * Time stamp in millis of the last time this Anim took a step, or 0 if we're
	 * just starting or restarting after a pause.
	 */
	protected long lastTime = 0;
	/**
	 * Total accumulated time spent computing the animation frames.
	 */
	protected long computeTime = 0;
	/**
	 * Total time this Anim has been running (including time between frames).
	 */
	protected long runningTime = 0;

	private Anim(boolean enabled) {
		if (enabled) {
			KeyFrame kf = new KeyFrame(Duration.millis(100), (ActionEvent event) -> {
				long t0 = System.currentTimeMillis();
				step(t0);
				long t = System.currentTimeMillis();
				computeTime += t - t0;
				if (lastTime != 0L)
					runningTime += t - lastTime;
				lastTime = t0;
				frameCount++;
			});
			timeline = new Timeline(kf);
			timeline.setCycleCount(Timeline.INDEFINITE);
		} else {
			timeline = null;
		}
	}

	public IAnimScript script(Predicate<Double> step) {
		return null;
	}

	public <T extends IPaintLayer> IGfxScript<T> script(T gfx, BiPredicate<T, Double> step) {
		return null;
	}

	/**
	 * Run an animation job.
	 * <p>
	 * The provided <code>step</code> function will be called at regular intervals,
	 * with the number of milliseconds since the last step as the integer argument.
	 * For the initial step and the first step after a pause, the argument will be a
	 * fixed value (normally 50ms).
	 * <p>
	 * The job is finished when <code>step</code> return <code>false</code>, or when
	 * {@link IAnimJob#stop()} is called on the returned object.
	 * <p>
	 * This is equivalent to calling {@link #play(Predicate, Object)} with
	 * <code>null</code> or <code>new Object()</code> as the second argument.
	 * <p>
	 * The job will start running at the next time step.
	 * 
	 * @param step
	 *            A function to be called at regular intervals
	 * @return An object that can be used to pause or stop the job or check the its
	 *         status
	 */
	public IAnimJob play(Predicate<Integer> step) {
		return play(step, new Object());
	}

	/**
	 * Run an animation job.
	 * <p>
	 * The provided <code>step</code> function will be called at regular intervals,
	 * with the number of milliseconds since the last step as the integer argument.
	 * For the initial step and the first step after a pause, the argument will be a
	 * fixed value (normally 50ms).
	 * <p>
	 * The job is finished when <code>step</code> return <code>false</code>, or when
	 * {@link IAnimJob#stop()} is called on the returned object.
	 * <p>
	 * The <code>exclude</code> argument can be used to avoid starting more than one
	 * of the same animation job. If a currently running job was submitted with the
	 * exact same object (==-identity), that job is returned instead and no new job
	 * is started. In this case, you can tell that the job is “old” by seeing that
	 * it has a non-zero {@link IAnimJob#getRunningTime()}. A <code>null</code>
	 * argument is equivalent to providing a new, unique object (i.e., no exclusion
	 * check is done).
	 * <p>
	 * The job will start running at the next time step.
	 * 
	 * @param step
	 *            A function to be called at regular intervals
	 * @param exclude
	 *            An exclusion object, to avoid starting the same job multiple times
	 * @return An object that can be used to pause or stop the job or check the its
	 *         status
	 */
	public IAnimJob play(Predicate<Integer> step, Object exclude) {
		if (step == null)
			throw new IllegalArgumentException();
		if (exclude == null)
			exclude = new Object();
		AnimJob job = jobs.get(exclude);
		if (job == null) {
			job = new AnimJob(step);
			jobs.put(exclude, job);
			startTimer();
		}
		return job;
	}

	protected void step(long currentTime) {
		Iterator<Entry<Object, AnimJob>> it = jobs.entrySet().iterator();
		while (it.hasNext()) {
			AnimJob job = it.next().getValue();
			switch (job.state) {
			case -1:
				break;
			case 0:
				it.remove();
			case 1:
				job.step(currentTime);
			}
		}
		if (jobs.isEmpty())
			stopTimer();
	}

	protected void startTimer() {
		if (timeline != null && timeline.getStatus() != Animation.Status.RUNNING) {
			lastTime = 0L;
			timeline.playFromStart();
		}
	}

	protected void stopTimer() {
		if (timeline != null && timeline.getStatus() == Animation.Status.RUNNING)
			timeline.stop();
	}

	public static Anim getInstance() {
		if (instance == null)
			instance = new Anim(true);
		return instance;
	}

	public static void disable() {
		if (instance != null)
			throw new IllegalStateException("must be called before getInstance()");
		instance = new Anim(false);
	}

	static class AnimJob implements IAnimJob {
		long lastStepTime = 0L;
		long runningTime = 0L;
		int state = 1;
		private Predicate<Integer> step;

		AnimJob(Predicate<Integer> step) {
			this.step = step;
		}

		void step(long currentTime) {
			try {
				int dt = 50;
				if (lastStepTime != 0L) {
					dt = (int) (currentTime - lastStepTime);
					runningTime += dt;
				}
				if (!step.test(dt))
					state = 0;
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lastStepTime = currentTime;
			}
		}

		@Override
		public boolean isRunning() {
			return state == 1;
		}

		@Override
		public boolean isPaused() {
			return state == -1;
		}

		@Override
		public boolean isStopped() {
			return state == 0;
		}

		@Override
		public void pause() {
			if (state == 1)
				state = -1;
		}

		@Override
		public void stop() {
			state = 0;
		}

		@Override
		public void unpause() {
			if (state == -1) {
				state = 1;
				lastStepTime = 0L;
			}
		}

		@Override
		public long getLastStepTime() {
			return lastStepTime;
		}

		@Override
		public long getRunningTime() {
			return runningTime;
		}
	}

	static class GfxScript<T extends IPaintLayer> extends Transition implements IGfxScript<T> {
		private T gfx;
		private Predicate<Double> step1;
		private BiPredicate<T, Double> step2;
		private IAnimScript next = null;

		GfxScript(Predicate<Double> step) {
			this.gfx = null;
			this.step1 = step;
			this.step2 = null;
		}

		GfxScript(T gfx, BiPredicate<T, Double> step) {
			this.gfx = gfx;
			this.step1 = null;
			this.step2 = step;
		}

		@Override
		public void play() {
			super.pause();
		}

		@Override
		public void stop() {
			super.pause();
		}

		@Override
		public void pause() {
			super.pause();
		}

		@Override
		public IAnimScript then(IAnimScript next) {
			this.next = next;
			return this;
		}

		@Override
		public IAnimScript setStartInterpolation(Interpolator startInterpolation) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnimScript setMidInterpolation(Interpolator midInterpolation) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnimScript setEndInterpolation(Interpolator endInterpolation) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnimScript play(T gfx) {
			this.gfx = gfx;
			play();
			return this;
		}

		@Override
		public T getGfx() {
			return gfx;
		}

		@Override
		public void setGfx(T gfx) {
			this.gfx = gfx;
		}

		@Override
		public IGfxScript<T> chain(IGfxScript<T> next) {
			this.next = next;
			return next;
		}

		@Override
		protected void interpolate(double arg0) {
			boolean r = false;
			if (step1 != null)
				r = step1.test(arg0);
			if (step2 != null)
				r = step2.test(gfx, arg0);
			if (!r)
				stop();
		}


		@Override
		public IAnimScript then() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnimScript transition(int millis, Consumer<Double> step) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public IAnimScript after(int millis) {
			// TODO Auto-generated method stub
			return null;
		}

	}
}
