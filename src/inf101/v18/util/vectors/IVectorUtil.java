package inf101.v18.util.vectors;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

public interface IVectorUtil {

	/**
	 * Elementwise addition of vector and scalar.
	 * 
	 * Performs <code>vec[i] += scalar</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scalar
	 *            A value to be added
	 * @param count
	 *            Number of elements in the vector
	 */
	void add(double[] vec, double scalar, int count);

	/**
	 * Elementwise scaled addition of two vectors.
	 * 
	 * Performs <code>vecA[i] += bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be added
	 * @param bScale
	 *            Scaling factor for elements to be added
	 * @param count
	 *            Number of elements in the vectors
	 */
	void add(double[] vecA, double[] vecB, double bScale, int count);

	/**
	 * Elementwise addition of two vectors.
	 * 
	 * Performs <code>vecA[i] += vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be added
	 * @param count
	 *            Number of elements in the vectors
	 */
	void add(double[] vecA, double[] vecB, int count);

	/**
	 * Elementwise division of vector with scalar.
	 * 
	 * Performs <code>vec[i] /= scale</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param divisor
	 *            Divisor
	 * @param count
	 *            Number of elements in the vector
	 */
	void div(double[] vec, double divisor, int count);

	/**
	 * Elementwise scaled division of two vectors.
	 * 
	 * Performs <code>vecA[i] /= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Divisor elements
	 * @param bScale
	 *            Scaling factor for divisor elements
	 * @param count
	 *            Number of elements in the vectors
	 */
	void div(double[] vecA, double[] vecB, double bScale, int count);

	/**
	 * Elementwise division of two vectors.
	 * 
	 * Performs <code>vecA[i] /= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Divisor elements
	 * @param count
	 *            Number of elements in the vectors
	 */
	void div(double[] vecA, double[] vecB, int count);

	/**
	 * Get element at an index.
	 * 
	 * Performs <code>vec[index] = value</code> for the given index.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param index
	 *            Index to set (must be 0 &leq; index &lt; min(count,
	 *            vec.length))
	 * @param count
	 *            Number of elements in the vector
	 * @return Value at vec[index]
	 */
	double get(double[] vec, int index, int count);

	/**
	 * Elementwise application of a binary function to a vector and a scalar.
	 * 
	 * Performs <code>vec[i] = fun.apply(vec[i], scalar)</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vec
	 *            The vector (overwritten with result)
	 * @param scalar
	 *            Second argument to function
	 * @param count
	 *            Number of elements in the vectors
	 */
	void map(DoubleBinaryOperator fun, double[] vec, double scalar, int count);

	/**
	 * Elementwise application of a binary function to two vectors.
	 * 
	 * Performs <code>vecA[i] = fun.apply(vecA[i], vecB[i])</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vecA
	 *            First vector (overwritten with result)
	 * @param vecB
	 *            Second vector
	 * @param count
	 *            Number of elements in the vectors
	 */
	void map(DoubleBinaryOperator fun, double[] vecA, double[] vecB, int count);

	/**
	 * Elementwise application of a unary function to a vector.
	 * 
	 * Performs <code>vec[i] = fun.apply(vec[i])</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vec
	 *            The vector (overwritten with result)
	 * @param scalar
	 *            Second argument to function
	 * @param count
	 *            Number of elements in the vectors
	 */
	void map(DoubleUnaryOperator fun, double[] vec, int count);

	/**
	 * Elementwise multiplication of vector and scalar.
	 * 
	 * Performs <code>vec[i] *= scale</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scale
	 *            Scaling factor
	 * @param count
	 *            Number of elements in the vector
	 */
	void mul(double[] vec, double scale, int count);

	/**
	 * Elementwise scaled multiplication of two vectors.
	 * 
	 * Performs <code>vecA[i] *= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be multiplied
	 * @param bScale
	 *            Scaling factor for elements to be multiplied
	 * @param count
	 *            Number of elements in the vectors
	 */
	void mul(double[] vecA, double[] vecB, double bScale, int count);

	/**
	 * Elementwise multiplication of two vectors.
	 * 
	 * Performs <code>vecA[i] *= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be multiplied
	 * @param count
	 *            Number of elements in the vectors
	 */
	void mul(double[] vecA, double[] vecB, int count);

	/**
	 * Set all elements to the same value.
	 * 
	 * Performs <code>vec[i] = value</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param value
	 *            Value to set
	 * @param count
	 *            Number of elements in the vector
	 */
	void fill(double[] vec, double value, int count);

	/**
	 * Fill a vector with elements from another vector.
	 * 
	 * Performs <code>vecA[i] = vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            Vector to initialize (overwritten with result)
	 * @param vecB
	 *            Vector to copy elements from
	 * @param count
	 *            Number of elements in the vectors
	 */
	void fill(double[] vecA, double[] vecB, int count);

	/**
	 * Set element at an index to some value.
	 * 
	 * Performs <code>vec[index] = value</code> for the given index.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param index
	 *            Index to set (must be 0 &leq; index &lt; min(count,
	 *            vec.length))
	 * @param value
	 *            Value to set
	 * @param count
	 *            Number of elements in the vector
	 */
	void set(double[] vec, int index, double value, int count);

	/**
	 * Elementwise subtraction of vector and scalar.
	 * 
	 * Performs <code>vec[i] -= scalar</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scalar
	 *            A value to be subtracted
	 * @param count
	 *            Number of elements in the vector
	 */
	void sub(double[] vec, double scalar, int count);

	/**
	 * Elementwise scaled subtraction of two vectors.
	 * 
	 * Performs <code>vecA[i] -= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be subtracted
	 * @param bScale
	 *            Scaling factor for elements to be subtracted
	 * @param count
	 *            Number of elements in the vectors
	 */
	void sub(double[] vecA, double[] vecB, double bScale, int count);

	/**
	 * Elementwise subtraction of two vectors.
	 * 
	 * Performs <code>vecA[i] -= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be subtracted
	 * @param count
	 *            Number of elements in the vectors
	 */
	void sub(double[] vecA, double[] vecB, int count);

}