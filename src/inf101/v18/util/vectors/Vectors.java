package inf101.v18.util.vectors;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

/**
 * Utility class for fast elementwise computations on vectors.
 * <p>
 * Each 'vector' is an array with an associated count of active elements. The
 * count can be less than the length of the array, and operations are only
 * performed on the first <code>count</code> elements.
 * <p>
 * Operations are available in both sequential and parallel versions; the
 * parallel versions are likely only faster on vectors with a large number of
 * elements, so they are selected only when <code>count</code> is larger than a
 * settable threshold ({@link #setThreshold(int)}). The default threshold is
 * {@value #DEFAULT_THRESHOLD} elements.
 * 
 * 
 * @author Anya Helene Bagge
 *
 */
public class Vectors {
	public static final IVectorUtil seqVectorUtil = new VectorUtilSeq();
	public static final IVectorUtil parVectorUtil = new VectorUtilParChunked();
	public static final int DEFAULT_THRESHOLD = 1000000;
	private static int threshold = DEFAULT_THRESHOLD;

	private Vectors() {
	}

	/**
	 * Set threshold for using parallel implementations
	 * 
	 * Parallel versions will be used for vectors larger than
	 * <code>threshold</code>
	 * 
	 * @param threshold
	 *            Vector length above which parallel computation will be
	 *            preferred
	 */
	public static void setThreshold(int threshold) {
		if (threshold < 0)
			throw new IllegalArgumentException("Threshold should be >= 0");

		Vectors.threshold = threshold;
	}

	/**
	 * Returns current threshold for selecting parallel operations.
	 */
	public static int getThreshold() {
		return Vectors.threshold;
	}

	/**
	 * Elementwise addition of vector and scalar.
	 * 
	 * Performs <code>vec[i] += scalar</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scalar
	 *            A value to be added
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void add(double[] vec, double scalar, int count) {
		if (count >= threshold)
			parVectorUtil.add(vec, scalar, count);
		else
			seqVectorUtil.add(vec, scalar, count);
	}

	/**
	 * Elementwise scaled addition of two vectors.
	 * 
	 * Performs <code>vecA[i] += bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be added
	 * @param bScale
	 *            Scaling factor for elements to be added
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void add(double[] vecA, double[] vecB, double bScale, int count) {
		if (count >= threshold)
			parVectorUtil.add(vecA, vecB, bScale, count);
		else
			seqVectorUtil.add(vecA, vecB, bScale, count);
	}

	/**
	 * Elementwise addition of two vectors.
	 * 
	 * Performs <code>vecA[i] += vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be added
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void add(double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.add(vecA, vecB, count);
		else
			seqVectorUtil.add(vecA, vecB, count);
	}

	/**
	 * Elementwise division of vector with scalar.
	 * 
	 * Performs <code>vec[i] /= scale</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param divisor
	 *            Divisor
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void div(double[] vec, double divisor, int count) {
		if (count >= threshold)
			parVectorUtil.div(vec, divisor, count);
		else
			seqVectorUtil.div(vec, divisor, count);
	}

	/**
	 * Elementwise scaled division of two vectors.
	 * 
	 * Performs <code>vecA[i] /= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Divisor elements
	 * @param bScale
	 *            Scaling factor for divisor elements
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void div(double[] vecA, double[] vecB, double bScale, int count) {
		if (count >= threshold)
			parVectorUtil.div(vecA, vecB, bScale, count);
		else
			seqVectorUtil.div(vecA, vecB, bScale, count);
	}

	/**
	 * Elementwise division of two vectors.
	 * 
	 * Performs <code>vecA[i] /= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Divisor elements
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void div(double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.div(vecA, vecB, count);
		else
			seqVectorUtil.div(vecA, vecB, count);
	}

	/**
	 * Get element at an index.
	 * 
	 * Performs <code>vec[index] = value</code> for the given index.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param index
	 *            Index to set (must be 0 &leq; index &lt; min(count,
	 *            vec.length))
	 * @param count
	 *            Number of elements in the vector
	 * @return Value at vec[index]
	 */
	public static double get(double[] vec, int index, int count) {
		if (count >= threshold)

			return parVectorUtil.get(vec, index, count);
		else
			return seqVectorUtil.get(vec, index, count);
	}

	/**
	 * Elementwise application of a binary function to a vector and a scalar.
	 * 
	 * Performs <code>vec[i] = fun.apply(vec[i], scalar)</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vec
	 *            The vector (overwritten with result)
	 * @param scalar
	 *            Second argument to function
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void map(DoubleBinaryOperator fun, double[] vec, double scalar, int count) {
		if (count >= threshold)

			parVectorUtil.map(fun, vec, scalar, count);
		else
			seqVectorUtil.map(fun, vec, scalar, count);
	}

	/**
	 * Elementwise application of a binary function to two vectors.
	 * 
	 * Performs <code>vecA[i] = fun.apply(vecA[i], vecB[i])</code> for all 0
	 * &leq; <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vecA
	 *            First vector (overwritten with result)
	 * @param vecB
	 *            Second vector
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void map(DoubleBinaryOperator fun, double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.map(fun, vecA, vecB, count);
		else
			seqVectorUtil.map(fun, vecA, vecB, count);
	}

	/**
	 * Elementwise application of a unary function to a vector.
	 * 
	 * Performs <code>vec[i] = fun.apply(vec[i])</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param fun
	 *            The function to be applied
	 * @param vec
	 *            The vector (overwritten with result)
	 * @param scalar
	 *            Second argument to function
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void map(DoubleUnaryOperator fun, double[] vec, int count) {
		if (count >= threshold)
			parVectorUtil.map(fun, vec, count);
		else
			seqVectorUtil.map(fun, vec, count);
	}

	/**
	 * Elementwise multiplication of vector and scalar.
	 * 
	 * Performs <code>vec[i] *= scale</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scale
	 *            Scaling factor
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void mul(double[] vec, double scale, int count) {
		if (count >= threshold)
			parVectorUtil.mul(vec, scale, count);
		else
			seqVectorUtil.mul(vec, scale, count);
	}

	/**
	 * Elementwise scaled multiplication of two vectors.
	 * 
	 * Performs <code>vecA[i] *= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be multiplied
	 * @param bScale
	 *            Scaling factor for elements to be multiplied
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void mul(double[] vecA, double[] vecB, double bScale, int count) {
		if (count >= threshold)
			parVectorUtil.mul(vecA, vecB, bScale, count);
		else
			seqVectorUtil.mul(vecA, vecB, bScale, count);
	}

	/**
	 * Elementwise multiplication of two vectors.
	 * 
	 * Performs <code>vecA[i] *= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be multiplied
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void mul(double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.mul(vecA, vecB, count);
		else
			seqVectorUtil.mul(vecA, vecB, count);
	}

	/**
	 * Set all elements to the same value.
	 * 
	 * Performs <code>vec[i] = value</code> for all 0 &leq; <code>i</code> &lt;
	 * <code>count</code>.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param value
	 *            Value to set
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void fill(double[] vec, double value, int count) {
		if (count >= threshold)
			parVectorUtil.fill(vec, value, count);
		else
			seqVectorUtil.fill(vec, value, count);
	}

	/**
	 * Fill a vector with elements from another vector.
	 * 
	 * Performs <code>vecA[i] = vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            Vector to initialize (overwritten with result)
	 * @param vecB
	 *            Vector to copy elements from
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void fill(double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.fill(vecA, vecB, count);
		else
			seqVectorUtil.fill(vecA, vecB, count);
	}

	/**
	 * Set element at an index to some value.
	 * 
	 * Performs <code>vec[index] = value</code> for the given index.
	 * 
	 * @param vec
	 *            Vector to initialize (overwritten with result)
	 * @param index
	 *            Index to set (must be 0 &leq; index &lt; min(count,
	 *            vec.length))
	 * @param value
	 *            Value to set
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void set(double[] vec, int index, double value, int count) {
		if (count >= threshold)
			parVectorUtil.set(vec, index, value, count);
		else
			seqVectorUtil.set(vec, index, value, count);
	}

	/**
	 * Elementwise subtraction of vector and scalar.
	 * 
	 * Performs <code>vec[i] -= scalar</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vec
	 *            An array of vector elements (overwritten with result)
	 * @param scalar
	 *            A value to be subtracted
	 * @param count
	 *            Number of elements in the vector
	 */
	public static void sub(double[] vec, double scalar, int count) {
		if (count >= threshold)
			parVectorUtil.sub(vec, scalar, count);
		else
			seqVectorUtil.sub(vec, scalar, count);
	}

	/**
	 * Elementwise scaled subtraction of two vectors.
	 * 
	 * Performs <code>vecA[i] -= bScale * vecB[i]</code> for all 0 &leq;
	 * <code>i</code> &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be subtracted
	 * @param bScale
	 *            Scaling factor for elements to be subtracted
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void sub(double[] vecA, double[] vecB, double bScale, int count) {
		if (count >= threshold)
			parVectorUtil.sub(vecA, vecB, bScale, count);
		else
			seqVectorUtil.sub(vecA, vecB, bScale, count);
	}

	/**
	 * Elementwise subtraction of two vectors.
	 * 
	 * Performs <code>vecA[i] -= vecB[i]</code> for all 0 &leq; <code>i</code>
	 * &lt; <code>count</code>.
	 * 
	 * @param vecA
	 *            An array of vector elements (overwritten with result)
	 * @param vecB
	 *            Elements to be subtracted
	 * @param count
	 *            Number of elements in the vectors
	 */
	public static void sub(double[] vecA, double[] vecB, int count) {
		if (count >= threshold)
			parVectorUtil.sub(vecA, vecB, count);
		else
			seqVectorUtil.sub(vecA, vecB, count);
	}

}
