package inf101.v18.gfx.particles.systems;

import inf101.v18.gfx.particles.ColorPainter;
import inf101.v18.gfx.particles.ParticleCanvas;
import inf101.v18.gfx.particles.ParticleGenerator;
import inf101.v18.gfx.particles.ParticleManager;
import inf101.v18.gfx.particles.RandomNumber;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.BoxBlur;
import javafx.scene.paint.Color;

public class Explosion extends ParticleCanvas {

	private double opacity = 0.1;
	private final ParticleManager manager;
	private double sizeFactor = 1.0;
	private double lifeTime = 1;
	private double diag;
	private Bloom bloomFilter = new Bloom();
	private BoxBlur blurFilter;
	private static final ColorPainter ORANGERED_YELLOW = new ColorPainter(Color.ORANGERED, Color.YELLOW);

	public Explosion() {
		this(256, 256);
	}

	public Explosion(double width, double height) {
		super(width, height);
		manager = new ParticleManager();
		diag = Math.sqrt(getNominalWidth() * getNominalHeight());
		blurFilter = new BoxBlur(diag / 128.0, diag / 128.0, 2);
		addManager(manager);
	}

	public void start() {
		manager.clear();

		manager.setMidPoint(.5, .5);
		manager.setMaxParticleSize(sizeFactor * diag / 20);
		ParticleGenerator master = new ParticleGenerator();
		master.setParticlesPerSecond(400);
		master.setOpacity(opacity);
		master.setDirection(90, 45, RandomNumber.Option.GAUSSIAN);
		master.setSpeed(diag / 10.0, diag / 60.0);
		master.setParticleSize(sizeFactor * diag / 40.0, sizeFactor * diag / 60);
		master.setXRelative(.5, .01);
		master.setYRelative(.5, .01);
		master.setLife(lifeTime, lifeTime / 4.0);
		master.setSpawnInterval(0.0, lifeTime * .7);

		ParticleGenerator gen;

		gen = master.copy();
		gen.setParticlePaint(ORANGERED_YELLOW);
		gen.setParticlesPerSecond(5000);
		gen.setSpawnInterval(0, 0.05);
		manager.addGenerator(gen);
	
		gen = master.copy();
		gen.setParticlePaint(ORANGERED_YELLOW);
		gen.setParticlesPerSecond(2500);
		gen.setSpawnInterval(.05, 0.10);
		manager.addGenerator(gen);
		
		
		gen = master.copy();
		gen.setParticlePaint(Color.ORANGE);
		gen.setSpawnInterval(0.3*lifeTime, 1*lifeTime);
		manager.addGenerator(gen);

		gen = master.copy();
		gen.setParticlePaint(ORANGERED_YELLOW);
		gen.setSpawnInterval(lifeTime * .1, lifeTime * .5);
		manager.addGenerator(gen);

		master.setParticlesPerSecond(100);
		master.setSpeed(diag / 20.0, diag / 60.0);
		master.setParticleSize(sizeFactor * diag / 20.0, sizeFactor * diag / 60);
//		master.setLife(lifeTime*2, lifeTime);
		master.setXRelative(.5, .015);
		master.setYRelative(.5, .015);

		gen = master.copy();
//		gen.setOpacity(opacity * 0.5);
		gen.setParticlePaint(Color.GRAY);
		gen.setSpawnInterval(0.0, lifeTime);
		manager.addGenerator(gen);

		master.setXRelative(.5, .025);
		master.setYRelative(.5, .025);

		gen = master.copy();
//		gen.setOpacity(opacity * 0.5);
		gen.setParticlePaint(Color.DARKGRAY);
		gen.setSpawnInterval(lifeTime/2, 1.1*lifeTime);
		manager.addGenerator(gen);
		

		// manager.setGravity(-diag/2000, 0.0);

		setExitHandler((x) -> start());
		bloomFilter.setInput(blurFilter);
		setEffect(bloomFilter);

		super.start();
	}
}
