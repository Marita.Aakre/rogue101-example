package inf101.v18.gfx.particles;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class ColorPainter implements IPainter {
	private final Color[] colors;

	public ColorPainter(Color color) {
		colors = new Color[1];
		colors[0] = color;
	}

	public ColorPainter(Color color, Color endColor) {
		colors = new Color[101];
		for (int i = 0; i < 101; i++) {
			colors[i] = color.interpolate(endColor, i / 100.0);
		}
	}

	@Override
	public Paint getPaint(double time) {
		if (colors.length == 101) {
			if (time < 0.0) {
				time = 0.0;
			}
			if (time > 1.0) {
				time = 1.0;
			}
			int c = (int) Math.round(time * 100.0);
			return colors[c];
		} else {
			return colors[0];
		}
	}

}
