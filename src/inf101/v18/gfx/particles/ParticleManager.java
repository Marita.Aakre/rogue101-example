package inf101.v18.gfx.particles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.animation.Interpolator;
import javafx.scene.canvas.GraphicsContext;

public class ParticleManager {
	protected List<ParticleGenerator> generators = new ArrayList<>();
	private double maxParticleSize = 400.0;
	private int maxParticles = 32768;
	private ParticleSet particles;
	private double xMidPoint = 0.5;
	private double yMidPoint = 0.5;
	private RandomNumber gravity;
	private boolean running = true;
	private boolean removeOutside = true;
	private static double wind = 0.0;

	static final ThreadLocal<Random> random = new ThreadLocal<Random>() {
		@Override
		protected Random initialValue() {
			return new Random();
		}
	};

	public ParticleManager() {
		particles = new ParticleSet(maxParticles);
	}

	public void addGenerator(ParticleGenerator gen) {
		generators.add(gen);
	}

	public void clear() {
		generators.clear();
	}

	public void clearGravity() {
		this.gravity = null;
	}

	public void drawParticles(ParticleCanvas canvas, double timeFactor, double timeStamp) {
		GraphicsContext context = canvas.getGraphicsContext2D();

		int pruned = 0;
		int outside = 0;
		int prunedOutside = 0;
		double width = canvas.getNominalWidth();
		double height = canvas.getNominalHeight();

		double xScale = canvas.getWidth() / width;
		double yScale = canvas.getHeight() / height;
		double screenScale = (xScale + yScale) / 2;

		context.save();
		context.translate(canvas.getWidth() * xMidPoint, canvas.getHeight() * yMidPoint);
		context.scale(screenScale, screenScale);

		for (int i = 0; i < particles.getSize(); i++) {
			double x = particles.getX(i);
			double y = particles.getY(i);
			double startTime = particles.get(ParticleProperty.TIMESTAMP, i);
			double life = particles.get(ParticleProperty.LIFE, i);
			double elapsed = timeStamp - startTime;
			if (elapsed > life) {
				pruned++;
				particles.removeParticle(i);
				// removeParticle will swap another particle into position i, so
				// we need to redo step i.
				i--;
				continue;
			} else if (x + maxParticleSize / 2.0 >= 0.0 && x - maxParticleSize / 2.0 <= width //
					&& y + maxParticleSize / 2.0 >= 0.0 && y - maxParticleSize / 2.0 <= height) //
			{
				double size = particles.getSize(i);
				double opacity = particles.get(ParticleProperty.OPACITY, i);

				double frac = elapsed / life;
				size = Interpolator.LINEAR.interpolate(size, maxParticleSize, frac);
				double interp = Interpolator.LINEAR.interpolate(1.0, 0.0, frac);
				double sizeFactor = 1.0 - (size * size) / (maxParticleSize * maxParticleSize);
				opacity = opacity * interp;
				opacity = opacity * sizeFactor;
				// opacity = opacity * interp * interp * interp;
				// opacity = opacity * opacity;
				 if (i == -1) // never happens
				 System.out.printf("%d x=%8.2f y=%8.2f frac=%4.2f size=%6.1f opacity=%6.1f time=%5.2f start=%5.2f elapsed=%5.2f life=%4.2f\n", i, x, y, frac, size, opacity*100.0,
				 timeStamp, startTime, elapsed, life);

				context.setGlobalAlpha(opacity);
				context.setFill(particles.getPainter(i).getPaint(frac));
				context.setEffect(null);

				// System.out.println("" + x + "," + y + "," + size);
				context.fillOval(x - size / 2.0 - width * xMidPoint, height - y - size / 2.0 - height * yMidPoint, size,
						size);
				// context.fillOval(x, y, size, size);
			} else if (removeOutside) {
				prunedOutside++;
				particles.removeParticle(i);
				// removeParticle will swap another particle into position i, so
				// we need to redo step i.
				i--;
				continue;
			} else {
				outside++;
			}
		}
		context.restore();

//		System.out.printf("  pruned: %4d pruned outside: %4d outside: %4d\n", pruned, prunedOutside, outside);
	}

	public int getMaxParticles() {
		return maxParticles;
	}

	public double getMaxParticleSize() {
		return maxParticleSize;
	}

	public int getNumParticles() {
		return particles.getSize();
	}

	public double getSpawnIntervalEnd() {
		double end = 0.0;
		for (ParticleGenerator g : generators) {
			end = Math.max(end, g.getSpawnIntervalEnd());
		}

		return end;
	}

	/**
	 * Called for every time step.
	 *
	 * Puts particles into the particle set, depending on the amount of time
	 * that has elapsed since last step.
	 *
	 * @param canvas
	 *            The canvas onto which the particles will be drawn
	 * @param elapsed
	 *            The number of seconds since last time handle was called
	 * @param timeStamp
	 *            Timestamp of current frame in seconds
	 */
	public void handle(ParticleCanvas canvas, double elapsed, double timeStamp) {
		if (!running) {
			return;
		}

//		System.out.printf("mem=%d elapsed=%.4f time=%.2f n=%d", Runtime.getRuntime().freeMemory(), elapsed, timeStamp,
//				particles.getSize());
		for (ParticleGenerator g : generators) {
			g.handle(canvas, particles, elapsed, timeStamp);
		}

		moveParticles(canvas, elapsed);

		if (timeStamp > getSpawnIntervalEnd() && getNumParticles() < 100) {
			running = false;
		}

		// System.out.println();
	}

	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}

	private void moveParticles(ParticleCanvas canvas, double timeFactor) {
		particles.move(timeFactor);
		if (gravity != null) {
			double g = gravity.next();
			if (g != 0.0) {
				particles.addTo(ParticleProperty.DY, -g*timeFactor);
			}
		}
		// particles.addTo(ParticleProperty.DX, wind);
		// particles.scaleSize(1 + timeFactor * growth);
		// particles.prune(ParticleProperty.X, -maxParticleSize,
		// canvas.getWidth() + maxParticleSize);
		// particles.prune(ParticleProperty.Y, -maxParticleSize,
		// canvas.getHeight() + maxParticleSize);

		wind = wind / 1.05;
		if (random.get().nextInt(10) == 0) {
			wind = wind + random.get().nextGaussian() / 50;
		}
	}

	public void setGravity(double base, double deviation, RandomNumber.Option... options) {
		this.gravity = RandomNumber.random(base, deviation, options);
	}

	/**
	 * Set the initial size of newly generated particles
	 *
	 * Takes effect immediately for the next generated particle.
	 *
	 * @param initialParticleSize
	 *            Initial size
	 * @param initialParticleDeviation
	 *            Standard deviation from initial size
	 */

	public void setMaxParticles(int maxParticles) {
		if (maxParticles < 1) {
			throw new IllegalArgumentException();
		}

		this.maxParticles = maxParticles;
		particles = new ParticleSet(maxParticles);
	}

	public void setMaxParticleSize(double maxParticleSize) {
		this.maxParticleSize = maxParticleSize;
	}

	public void setMidPoint(double x, double y) {
		xMidPoint = x;
		yMidPoint = y;
	}

	public void start() {
		// Timeline tl = new Timeline();
		// tl.getKeyFrames().add(new KeyFrame(Duration.millis(1000), ae -> {
		// System.out.println("mem: " + Runtime.getRuntime().freeMemory() + "
		// objs: " + particles.getSize());
		// tl.playFromStart();
		// }));
		// tl.play();

		running = true;
		particles.clear();
	}
}
