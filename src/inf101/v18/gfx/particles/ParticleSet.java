package inf101.v18.gfx.particles;

import inf101.v18.util.vectors.Vectors;
import javafx.scene.paint.Color;

public class ParticleSet {
	private int maxCount;
	private int count;
	private double[] xPos;
	private double[] yPos;
	private double[] size;
	private double[] dx;
	private double[] dy;
	private double[] life;
	private double[] timestamp;
	private double[] opacity;
	private IPainter[] painter;
	private static final IPainter DEFAULT_PAINTER = new ColorPainter(Color.WHITE);

	public ParticleSet(int maxCount) {
		this.maxCount = maxCount;
		this.xPos = new double[maxCount];
		this.yPos = new double[maxCount];
		this.size = new double[maxCount];
		this.dx = new double[maxCount];
		this.dy = new double[maxCount];
		this.life = new double[maxCount];
		this.timestamp = new double[maxCount];
		this.opacity = new double[maxCount];
		this.painter = new IPainter[maxCount];
	}

	public int addParticle(double time, double x, double y, double sz) {
		if (!isFull()) {
			xPos[count] = x;
			yPos[count] = y;
			size[count] = sz;
			dx[count] = 0.0;
			dy[count] = 0.0;
			life[count] = 0;
			timestamp[count] = time;
			opacity[count] = 1.0;
			painter[count] = DEFAULT_PAINTER;

			count++;
			return count - 1;
		} else {
			return -1;
		}

	}

	public void addTo(ParticleProperty prop, double scalar) {
		Vectors.add(getArray(prop), scalar, count);
	}

	public void addToSize(double delta) {
		Vectors.add(size, delta, count);
	}

	public void clear() {
		count = 0;
	}

	public double get(ParticleProperty prop, int index) {
		return getArray(prop)[index];
	}

	private double[] getArray(ParticleProperty prop) {
		switch (prop) {
		case DX:
			return dx;
		case DY:
			return dy;
		case TIMESTAMP:
			return timestamp;
		case OPACITY:
			return opacity;
		case LIFE:
			return life;
		case SIZE:
			return size;
		case X:
			return xPos;
		case Y:
			return yPos;
		default:
			throw new IllegalArgumentException(prop.toString());
		}
	}

	public IPainter getPainter(int i) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		return painter[i];
	}

	public int getSize() {
		return count;
	}

	public double getSize(int i) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		return size[i];
	}

	public double getX(int i) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		return xPos[i];
	}

	public double getY(int i) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		return yPos[i];
	}

	public boolean isEmpty() {
		return count == 0;
	}

	public boolean isFull() {
		return count == maxCount;
	}

	public int maxSize() {
		return maxCount;
	}

	public void move() {
		Vectors.add(xPos, dx, count);
		Vectors.add(yPos, dy, count);
	}

	public void move(double factor) {
		Vectors.add(xPos, dx, factor, count);
		Vectors.add(yPos, dy, factor, count);
	}

	private int prune(double[] array, double minAllowed, double maxAllowed) {
		int c = 0;
		for (int i = 0; i < count; i++) {
			if (array[i] < minAllowed || array[i] > maxAllowed) {
				removeParticle(i);
				c++;
			}
		}

		return c;
	}

	public int prune(ParticleProperty prop, double minAllowed, double maxAllowed) {
		return prune(getArray(prop), minAllowed, maxAllowed);
	}

	public void removeParticle(int index) {
		if (index >= count) {
			throw new IndexOutOfBoundsException(String.valueOf(index));
		}

		if (index != count - 1) {
			xPos[index] = xPos[count - 1];
			yPos[index] = yPos[count - 1];
			size[index] = size[count - 1];
			dx[index] = dx[count - 1];
			dy[index] = dy[count - 1];
			life[index] = life[count - 1];
			timestamp[index] = timestamp[count - 1];
			opacity[index] = opacity[count - 1];
			painter[index] = painter[count - 1];

		}
		count--;
	}

	public void scale(ParticleProperty prop, double scale) {
		Vectors.mul(getArray(prop), scale, count);
	}

	public void scaleSize(double factor) {
		Vectors.mul(size, factor, count);
	}

	public void set(ParticleProperty prop, int index, double newValue) {
		getArray(prop)[index] = newValue;
	}

	public void setAge(int i, double dy) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		this.life[i] = dy;
	}

	public void setAll(ParticleProperty prop, double newValue) {
		Vectors.fill(getArray(prop), newValue, count);
	}

	public void setDX(int i, double dx) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		this.dx[i] = dx;
	}

	public void setDY(int i, double dy) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		this.dy[i] = dy;
	}

	public void setPainter(int i, IPainter painter) {
		if (i < 0 || i >= count) {
			throw new IllegalArgumentException(String.valueOf(i));
		}
		this.painter[i] = painter;
	}

}
