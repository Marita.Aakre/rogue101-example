package inf101.v18.gfx.particles;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import javafx.animation.AnimationTimer;
import javafx.animation.Interpolator;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Blend;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.ColorInput;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

public class ParticleCanvas extends Canvas {
	private double normalTimeStep = 1.0 / 60.0;
	private double fpsElapsed = 0;
	private volatile double fastForward = 0.0;
	private double timeAdjust = 0.0;
	private boolean running = false;
	private int fpsCount = 0;
	private Random random = new Random();
	// private static final long serialVersionUID = -5830018712727696869L;
	private AnimationTimer timer;

	private long lastUpdateTime = 0L;
	private int frameCount = 0;
	private int motionBlur = 5;
	private double firstUpdateTime = -1.0;
	private double nominalWidth;
	private double nominalHeight;
	private Consumer<ParticleCanvas> exitHandler = null;
	private final Canvas canvas;
	protected List<ParticleManager> managers = new ArrayList<>();
	private static final double MILLI = 1000.0;

	private static final double MICRO = 1000.0 * MILLI;

	private static final double NANO = 1000.0 * MICRO;

	public ParticleCanvas() {
		super();

		nominalWidth = getWidth();
		nominalHeight = getHeight();

		canvas = new Canvas(nominalWidth, nominalHeight);
	}

	public ParticleCanvas(double width, double height) {
		super(width, height);

		nominalWidth = getWidth();
		nominalHeight = getHeight();

		canvas = new Canvas(nominalWidth, nominalHeight);
	}

	public void addManager(ParticleManager manager) {
		managers.add(manager);
	}

	public void fastForward(double seconds) {
		fastForward = seconds;
	}

	public Consumer<ParticleCanvas> getExitHandler() {
		return exitHandler;
	}

	public double getNominalHeight() {
		return nominalHeight;
	}

	public double getNominalWidth() {
		return nominalWidth;
	}

	public boolean isRunning() {
		return running;
	}

	public void setExitHandler(Consumer<ParticleCanvas> exitHandler) {
		this.exitHandler = exitHandler;
	}

	public void start() {
		// canvas.setEffect(new BoxBlur(10, 10, 2));
		setOpacity(1.0);

		for (ParticleManager mgr : managers) {
			mgr.start();
		}

		timer = new AnimationTimer() {
			@Override
			public void handle(long timestamp) {
				if (firstUpdateTime < 0) {
					firstUpdateTime = timestamp;
					lastUpdateTime = 0L;
					timeAdjust = 0.0;
					running = true;
					frameCount = 0;
					GraphicsContext context = getGraphicsContext2D();
					context.clearRect(0, 0, getWidth(), getHeight());
				}

				if (frameCount % 3 == 0) {
					if (lastUpdateTime > 0) {
						if (fastForward > 0.0) {
							while (fastForward > 0.0) {
								for (ParticleManager g : managers) {
									g.handle(ParticleCanvas.this, normalTimeStep,
											timeAdjust + (timestamp - firstUpdateTime) / NANO);
								}
								timeAdjust += normalTimeStep;
								fastForward -= normalTimeStep;
							}
							fastForward = 0.0;
						} else {
							long elapsedSinceLast = timestamp - lastUpdateTime;

							GraphicsContext context = getGraphicsContext2D();
							context.setGlobalAlpha(0.1);
							context.clearRect(0, 0, getWidth(), getHeight());

							boolean r = false;
							double time = timeAdjust + (lastUpdateTime - firstUpdateTime) / NANO;
							double deltaT = elapsedSinceLast / NANO;

							for (ParticleManager g : managers) {
								g.drawParticles(ParticleCanvas.this, 0.0, time);
							}
							for (ParticleManager g : managers) {
								g.handle(ParticleCanvas.this, deltaT, time);
								g.drawParticles(ParticleCanvas.this, deltaT, time);
								r |= g.isRunning();
							}

							if (!r) {
								running = false;
								timer.stop();
								firstUpdateTime = -1.0;
								if (exitHandler != null) {
									exitHandler.accept(ParticleCanvas.this);
								}
							}
						}
					}
					lastUpdateTime = timestamp;
				}
				frameCount++;
			}
		};
		timer.start();

	}

	public void stop() {
		timer.stop();
	}
}
